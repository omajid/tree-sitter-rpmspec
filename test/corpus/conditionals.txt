===============================================================================
Conditionals (%ifarch)
===============================================================================

%ifarch s390x
BuildRequires:  foo
%endif

-------------------------------------------------------------------------------

(spec
  (if_statement
    (preamble
      (tags
        (dependency_tag)
        (string
          (string_content))))))

===============================================================================
Conditionals (%ifos)
===============================================================================

%ifos linux
BuildRequires:  foo
%endif

-------------------------------------------------------------------------------

(spec
  (if_statement
    (preamble
      (tags
        (dependency_tag)
        (string
          (string_content))))))

===============================================================================
Conditionals (%ifnarch)
===============================================================================

%ifnarch x86_64
BuildRequires:  magic
%endif

-------------------------------------------------------------------------------

(spec
  (if_statement
    (preamble
      (tags
        (dependency_tag)
        (string
          (string_content))))))

===============================================================================
Conditionals (%if with simple macros)
===============================================================================

%if %{defined with_foo}
BuildRequires:  foo
%endif

-------------------------------------------------------------------------------

(spec
  (if_statement
    (preamble
      (tags
        (dependency_tag)
        (string
          (string_content))))))

===============================================================================
Conditionals (%if with complex macros)
===============================================================================

%if %{defined with_foo} && %{undefined with_bar}
BuildRequires:  foo
%endif

-------------------------------------------------------------------------------

(spec
  (if_statement
    (preamble
      (tags
        (dependency_tag)
          (string
            (string_content))))))

===============================================================================
Conditionals (%if with string comparison)
===============================================================================

%if "%{optimize_flags}" != "none"
BuildRequires:  foo
%endif

-------------------------------------------------------------------------------

(spec
  (if_statement
    (preamble
      (tags
        (dependency_tag)
        (string
          (string_content))))))

===============================================================================
Conditionals (%if with mathematical comparison)
===============================================================================

%if 0%{?fedora} > 10 || 0%{?rhel} > 7
BuildRequires:  foo
%endif

-------------------------------------------------------------------------------

(spec
  (if_statement
    (preamble
      (tags
        (dependency_tag)
        (string
          (string_content))))))

===============================================================================
Conditionals (%if with expansion)
===============================================================================

%if 0%{?fedora}
BuildRequires:  foo
%endif

-------------------------------------------------------------------------------

(spec
  (if_statement
    (preamble
      (tags
        (dependency_tag)
        (string
          (string_content))))))

===============================================================================
Conditionals (%if with %else)
===============================================================================

%if 0%{?fedora}
BuildRequires:  foo
%else
BuildRequires:  bar
%endif

-------------------------------------------------------------------------------

(spec
  (if_statement
    (preamble
      (tags
        (dependency_tag)
        (string
          (string_content))))
    (else_clause
      (preamble
        (tags
          (dependency_tag)
          (string
            (string_content)))))))

===============================================================================
Conditionals (%if with %elif)
===============================================================================

%if 0%{?fedora}
BuildRequires:  foo
%elif 0%{?rhel}
BuildRequires:  bar
%endif

-------------------------------------------------------------------------------

(spec
  (if_statement
    (preamble
      (tags
        (dependency_tag)
        (string
          (string_content))))
    (elif_clause
      (preamble
        (tags
          (dependency_tag)
          (string
            (string_content)))))))

===============================================================================
Conditionals (%if with %elif and %else)
===============================================================================

%if 0%{?fedora}
BuildRequires:  foo
%elif 0%{?rhel}
BuildRequires:  bar
%else
BuildRequires:  wurst
%endif

-------------------------------------------------------------------------------

(spec
  (if_statement
    (preamble
      (tags
        (dependency_tag)
        (string
          (string_content))))
    (elif_clause
      (preamble
        (tags
          (dependency_tag)
          (string
            (string_content)))))
    (else_clause
      (preamble
        (tags
          (dependency_tag)
          (string
            (string_content)))))))

===============================================================================
Conditionals (Nested %if with %elif and %else)
===============================================================================

%if 0%{?rhel}

%ifarch x86_64
%bcond_without winexe
%else
%bcond_with winexe
#endifarch
%endif

%else
%bcond_without winexe
%endif

-------------------------------------------------------------------------------

(spec
  (if_statement
    (if_statement
      (macro_invocation
        (macro_expansion
          (variable_name))
        (string
          (string_content)))
      (else_clause
        (macro_invocation
          (macro_expansion
            (variable_name))
          (string
            (string_content))))
      (comment))
    (else_clause
      (macro_invocation
        (macro_expansion
          (variable_name))
        (string
          (string_content))))))

===============================================================================
Conditionals (%if %{with ..})
===============================================================================

%if %{with libwbclient}
Requires(post): libwbclient = %{samba_depver}
%endif

-------------------------------------------------------------------------------

(spec
  (if_statement
    (preamble
      (tags
        (dependency_tag
          (qualifier))
        (string
          (string_content)
          (macro_expansion
            (variable_name)))))))
